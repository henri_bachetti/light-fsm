
#include <Arduino.h>
#include "light-fsm.h"

static struct machineState *mState;
static int state;
LIST_HEAD(head);

// startMachine : starts the engine
// ms : the machine state / transitions table. The table must be NULL terminated
// st : the initial state
void startMachine(struct machineState *ms, int st)
{
  mState = ms;
  state = st;
}

static void fireEvent(int event, int arg)
{
  int n;

  if (mState == 0) {
    return;
  }
  Serial.print(F("state ")); Serial.print(state); Serial.print(F(" | event ")); Serial.print(event);
  Serial.print(F("(")); Serial.print(arg); Serial.println(F(")"));
  for (n = 0 ; mState[n].currentState != 0 ; n++) {
    struct machineState *ms = &mState[n];
    if ((ms->currentState == IGNORE || ms->currentState == state) && (ms->event == IGNORE || ms->event == event)) {
      if (ms->arg == IGNORE || ms->arg == arg) {
        if (ms->action != NO_ACTION) {
          ms->action(arg);
        }
        if (ms->nextState != STATE_NOCHANGE) {
          state = ms->nextState;
          Serial.print(F("NEXT STATE ")); Serial.println(state);
        }
        else {
          Serial.println(F("NO STATE CHANGE"));
        }
        return;
      }
    }
  }
  Serial.print(F("??? (")); Serial.print(n); Serial.println(F(")"));
  delay(500);
}

static int countEvents(void)
{
  register struct list_head *p;
  int cnt = 0;

  for (p = head.next ; p != &head ; p = p->next) {
    cnt++;
  }
  return cnt;
}

// postEvent posts an event
// e: the event structure
// event : the event ID
// arg : an argument
void postEvent(struct event *e, int event, int arg)
{
  //  Serial.print(F("post EVENT ")); Serial.print(event);
  e->event = event;
  e->arg = arg;
  list_add_tail(&e->entry, &head);
  //  Serial.print(F(" (")); Serial.print(countEvents()); Serial.println(F(")"));
}

// processEvents processes an event from the event queue
// this function must be called from loop()
void processEvents(void)
{
  register struct event *e;
  register struct list_head *p;

  p = head.next;
  if (p != &head) {
    e = list_entry(p, struct event, entry);
    //    Serial.print(F("got EVENT ")); Serial.println(e->event);
    fireEvent(e->event, e->arg);
    list_del(&e->entry);
  }
}

// getMachineState returns the current engine state
int getMachineState(void)
{
  return state;
}

