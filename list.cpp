
#include "list.h"

void __list_add(struct list_head *list, struct list_head *prev, struct list_head *next)
{
  next->prev = list;
  list->next = next;
  list->prev = prev;
  prev->next = list;
}

void list_add(struct list_head *list, struct list_head *head)
{
  __list_add(list, head, head->next);
}

void list_add_tail(struct list_head *list, struct list_head *head)
{
  __list_add(list, head->prev, head);
}

void __list_del(struct list_head *prev, struct list_head *next)
{
  next->prev = prev;
  prev->next = next;
}

void list_del(struct list_head *entry)
{
  __list_del(entry->prev, entry->next);
  entry->next = LIST_POISON1;
  entry->prev = LIST_POISON2;
}

int list_empty(const struct list_head *head)
{
  return head->next == head;
}

