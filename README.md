# LIGHT-FSM

This is a Light Finite State Machine for ARDUINO, or other platforms like ESP8266 or ESP32.
This library provides some examples.

## Structures

```
struct event
{
  struct list_head entry; // the entry in the queue
  int event;              // the event ID
  int arg;                // an argument
};

struct machineState
{
  int currentState;       // the current state
  int event;              // an event ID
  int arg;                // an argument
  int (*action)(int arg); // an action
  int nextState;          // the next state
};
```

## Functions

**void startMachine(struct machineState *ms, int st);**

Starts the engine.

ms : the machine state / transitions table. The table must be NULL terminated

st : the initial state

**void postEvent(struct event *e, int event, int arg);**

Posts an event in the events queue.

e: the event structure

event : the event ID

arg : an argument

**int getMachineState(void);**

Returns the current engine state

**void processEvents(void);**

Processes an event from the event queue.

This function must be called from loop()

And don't forget to have a look to the examples.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2019/11/arduino-un-moteur-de-machine-etats.html

