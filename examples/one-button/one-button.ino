
#include <Bounce2.h>

#include "light-fsm.h"

#define BUTTON_PIN              2
#if defined ESP8266 || defined ESP32
#define LED_BUILTIN             4
#endif

#define buttonIsPressed()       (debouncer.read() == LOW ? true : false)

#define STATE_OFF               1
#define STATE_ON                2

#define EVENT_BTN               1

int ledOn(int arg);
int ledOff(int arg);

static struct machineState states[] =
{
  {STATE_OFF,  EVENT_BTN,    true,   ledOn,     STATE_ON},
  {STATE_ON,   EVENT_BTN,    true,   ledOff,    STATE_OFF},
  {IGNORE,     EVENT_BTN,    false,  NO_ACTION, STATE_NOCHANGE},
  0
};

Bounce debouncer = Bounce(); 
struct event buttonEvent;


int ledOn(int arg)
{
  Serial.println("LED ON");
  digitalWrite(LED_BUILTIN, HIGH);
}

int ledOff(int arg)
{
  Serial.println("LED OFF");
  digitalWrite(LED_BUILTIN, LOW);
}

void readButton(void)
{
  static int state = false;

  int btn = buttonIsPressed();
  if (state != btn) {
    postEvent(&buttonEvent, EVENT_BTN, btn);
    state = btn;
  }
}

void setup(void)
{
  Serial.begin(115200);
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  pinMode(LED_BUILTIN, OUTPUT);
  debouncer.attach(BUTTON_PIN);
  debouncer.interval(5);
  Serial.println("LIGHT STATE MACHINE DEMO");
  startMachine(states, STATE_OFF);
}

void loop(void)
{
  debouncer.update();
  readButton();
  processEvents();
}

