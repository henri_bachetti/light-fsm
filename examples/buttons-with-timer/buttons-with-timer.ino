
#include <Bounce2.h>

#include "light-fsm.h"

#define BUTTON1                 0
#define BUTTON2                 1
#define BUTTON1_PIN             2
#define BUTTON2_PIN             3
#if defined ESP8266 || defined ESP32
#define LED_BUILTIN             4
#endif

#define STATE_OFF               1
#define STATE_ON                2

#define EVENT_BTN1              1
#define EVENT_BTN2              2
#define EVENT_BTNLP1            3
#define EVENT_BTNLP2            4
#define EVENT_TIMER             5

int buttonEvtNumber[] = {EVENT_BTN1, EVENT_BTN2};
int buttonEvtLpNumber[] = {EVENT_BTNLP1, EVENT_BTNLP2};

int ledOn(int arg);
int ledOff(int arg);

static struct machineState states[] =
{
  {STATE_OFF,  EVENT_BTN1,     true,   ledOn,      STATE_ON},
  {STATE_OFF,  EVENT_BTN2,     true,   ledOn,      STATE_ON},
  {STATE_OFF,  EVENT_BTNLP1,   IGNORE, NO_ACTION,  STATE_NOCHANGE},
  {STATE_OFF,  EVENT_BTNLP2,   IGNORE, NO_ACTION,  STATE_NOCHANGE},
  {STATE_ON,   EVENT_BTN1,     IGNORE, NO_ACTION,  STATE_NOCHANGE},
  {STATE_ON,   EVENT_BTN2,     true,   ledOff,     STATE_OFF},
  {STATE_ON,   EVENT_BTNLP1,   true,   ledOff,     STATE_OFF},
  {STATE_ON,   EVENT_BTNLP2,   IGNORE, NO_ACTION,  STATE_NOCHANGE},
  {STATE_ON,   EVENT_TIMER,    IGNORE, ledOff,     STATE_OFF},
  0
};

Bounce debouncer1 = Bounce();
Bounce debouncer2 = Bounce();
Bounce *debouncer[] = {&debouncer1, &debouncer2};

#define buttonIsPressed(button) (debouncer[button]->read() == LOW ? true : false)

struct event buttonEvent;
struct event longPressEvent;
struct event timerEvent;

unsigned long timer;

int ledOn(int arg)
{
  Serial.println("LED ON");
  digitalWrite(LED_BUILTIN, HIGH);
  timer = millis();
}

int ledOff(int arg)
{
  Serial.println("LED OFF");
  digitalWrite(LED_BUILTIN, LOW);
  timer = 0;
}

void readButton(int button)
{
  static int state[2] = {false, false};
  static unsigned long pressTime[2];

  int btn = buttonIsPressed(button);
  if (state[button] != btn) {
    if (btn == true) {
      if (pressTime[button] == 0) {
        pressTime[button] = millis();
      }
    }
    else {
      if (pressTime[button] != 0 && millis() - pressTime[button] > 2000) {
        Serial.println(F("LONG PRESS"));
        postEvent(&longPressEvent, buttonEvtLpNumber[button], true);
        pressTime[button] = 0;
      }
      else {
        postEvent(&buttonEvent, buttonEvtNumber[button], true);
      }
      pressTime[button] = 0;
    }
    state[button] = btn;
  }
}

int readTimer(void)
{
  if (timer != 0 && millis() - timer > 30000) {
    postEvent(&timerEvent, EVENT_TIMER, 0);
  }
}

void setup(void)
{
  Serial.begin(115200);
  Serial.println("LIGHT STATE MACHINE DEMO");
  pinMode(BUTTON1_PIN, INPUT_PULLUP);
  pinMode(BUTTON2_PIN, INPUT_PULLUP);
  pinMode(LED_BUILTIN, OUTPUT);
  debouncer1.attach(BUTTON1_PIN);
  debouncer1.interval(5);
  debouncer2.attach(BUTTON2_PIN);
  debouncer2.interval(5);
  startMachine(states, STATE_OFF);
}

void loop(void)
{
  debouncer1.update();
  debouncer2.update();
  readButton(BUTTON1);
  readButton(BUTTON2);
  readTimer();
  processEvents();
}


