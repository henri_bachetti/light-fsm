
#include <Bounce2.h>

#include "light-fsm.h"

#define BUTTON_PIN              2
#if defined ESP8266 || defined ESP32
#define LED_BUILTIN             4
#endif
#define V_BATT                  0
#define VREF                    1.078
#define BATT_LIMIT              3.0
#define BATT_DIVIDER            0.205

#define BLINK_ON_TIME           50
#define BLINK_OFF_TIME          4000

#define buttonIsPressed()       (debouncer.read() == LOW ? true : false)

#define STATE_OFF               1
#define STATE_ON                2
#define STATE_INACTIVE          3

#define EVENT_BTN               1
#define EVENT_BATT              2
#define EVENT_TIMER             3

int ledOn(int arg);
int ledOff(int arg);
int startBlink(void);
int stopBlink(void);
int blinkOn(void);
int blinkOff(void);

static unsigned long timer;
static unsigned long duration;

static struct machineState states[] =
{
  {STATE_OFF,       EVENT_BTN,    true,           ledOn,      STATE_ON},
  {STATE_ON,        EVENT_BTN,    true,           ledOff,     STATE_OFF},
  {IGNORE,          EVENT_BATT,   LOW,            startBlink, STATE_INACTIVE},
  {IGNORE,          EVENT_BATT,   HIGH,           stopBlink,  STATE_OFF},
  {STATE_INACTIVE,  EVENT_BTN,    true,           NO_ACTION,  STATE_NOCHANGE},
  {STATE_INACTIVE,  EVENT_TIMER,  BLINK_ON_TIME,  blinkOff,   STATE_NOCHANGE},
  {STATE_INACTIVE,  EVENT_TIMER,  BLINK_OFF_TIME, blinkOn,    STATE_NOCHANGE},
  {IGNORE,          EVENT_BTN,    false,          NO_ACTION,  STATE_NOCHANGE},
  0
};

Bounce debouncer = Bounce();
struct event battEvent;
struct event buttonEvent;

int ledOn(int arg)
{
  Serial.println("LED ON");
  digitalWrite(LED_BUILTIN, HIGH);
}

int ledOff(int arg)
{
  Serial.println("LED OFF");
  digitalWrite(LED_BUILTIN, LOW);
}

int startBlink(void)
{
  Serial.println("START BLINK");
  digitalWrite(LED_BUILTIN, HIGH);
  duration = BLINK_ON_TIME;
  timer = millis();
}

int stopBlink(void)
{
  Serial.println("STOP BLINK");
  digitalWrite(LED_BUILTIN, LOW);
  timer = 0;
}

int blinkOn(void)
{
  Serial.println("BLINK ON");
  digitalWrite(LED_BUILTIN, HIGH);
  duration = BLINK_ON_TIME;
}

int blinkOff(void)
{
  Serial.println("BLINK OFF");
  digitalWrite(LED_BUILTIN, LOW);
  duration = BLINK_OFF_TIME;
}

float getBatteryVoltage(void)
{
  unsigned int adc;

  adc = analogRead(V_BATT);
  return adc * VREF / 1023 / BATT_DIVIDER;
}

int readBatteryState(void)
{
  static int state = HIGH;
  float vBatt;

  vBatt = getBatteryVoltage();
  int batt = vBatt > BATT_LIMIT ? HIGH : LOW;
  if (batt != state) {
    state = batt;
    Serial.print(F("BATT: ")); Serial.print(vBatt); Serial.print(F("V")); Serial.println(vBatt < BATT_LIMIT ? F(" LOW") : F(" OK"));
    return batt;
  }
  return -1;
}

void readButton(void)
{
  static int state = false;

  int btn = buttonIsPressed();
  if (state != btn) {
    postEvent(&buttonEvent, EVENT_BTN, btn);
    state = btn;
  }
}

void runTimer(void)
{
  if (timer == 0) {
    return;
  }
  if (millis() - timer > duration) {
    postEvent(&battEvent, EVENT_TIMER, duration);
    timer = millis();
  }
}

void setup(void)
{
  Serial.begin(115200);
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  pinMode(LED_BUILTIN, OUTPUT);
  debouncer.attach(BUTTON_PIN);
  debouncer.interval(5);
  Serial.println("LIGHT STATE MACHINE DEMO");
  analogReference(INTERNAL);
  startMachine(states, STATE_OFF);
}

void loop(void)
{
  int batt = readBatteryState();
  if (batt != -1) {
    postEvent(&battEvent, EVENT_BATT, batt);
  }
  runTimer();
  debouncer.update();
  readButton();
  processEvents();
}

