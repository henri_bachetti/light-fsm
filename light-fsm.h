
#ifndef _MACHINE_H_
#define _MACHINE_H_

#include "list.h"

#define STATE_NOCHANGE      0
#define IGNORE              -1
#define NO_ACTION           0

struct event
{
  struct list_head entry;
  int event;
  int arg;
};

struct machineState
{
  int currentState;
  int event;
  int arg;
  int (*action)(int arg);
  int nextState;
};

void startMachine(struct machineState *ms, int st);
void fireEvent(int event, int arg);
void postEvent(struct event *e, int event, int arg);
void processEvents(void);
int getMachineState(void);

#endif

